package com.profile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Map;

@RestController
@RequestMapping
public class ProfileController {
    @Autowired
    private Profile profile;

    @GetMapping("/users")
    @ResponseBody
    public Collection<User> getAll() {
        return profile.getAllUsers();
    }

    @PostMapping("/add")
    public ResponseEntity<Map<Integer, User>> addUser(@RequestBody User user) {
        Map<Integer, User> response = profile.addUser(user.firstname, user.lastname, user.phone, user.address);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Map<Integer, User>> updateUser(@PathVariable int id, @RequestBody User user) {
        Map<Integer, User> response = profile.updateUser(id, user.firstname, user.lastname, user.phone, user.address);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map<Integer, User>> deleteUser(@PathVariable int id) {
        Map<Integer, User> response = profile.removeUser(id);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/all")
    public ResponseEntity<Map<Integer, User>> deleteAll(@RequestBody User user) {
        Map<Integer, User> response = profile.removeAllUsers();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}