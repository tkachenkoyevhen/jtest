package com.profile;

import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class Profile {
    Map<Integer, User> store = new ConcurrentHashMap<>();

    public Map<Integer, User> addUser(String firstname, String lastname, String phone, String email) {
        Integer id = 1;
        if (!store.isEmpty()) {
            id = store.keySet().size() + 1;
        }
        User user = new User(id, firstname, lastname, phone, email);
        store.put(id, user);

        return store;
    }

    public Map<Integer, User> updateUser(int id, String firstname, String lastname, String phone, String email) {
        User user = new User(id, firstname, lastname, phone, email);

        if(!store.isEmpty())
            store.replace(id, user);
        return store;
    }

    public Map<Integer, User> removeUser(int id) {
        if(store.containsKey(id))
            store.remove(id);
        return store;
    }

    public Map<Integer, User> removeAllUsers() {
        if(!store.isEmpty())
            store.clear();
        return store;
    }

    public Collection<User> getAllUsers() {
        return store.values();
    }
}
