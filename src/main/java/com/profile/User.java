package com.profile;

public class User {

    public String firstname;
    public String lastname;
    public String phone;
    public String address;
    public Integer id;

    @Override
    public String toString() {
        return "User{" +
                "  id=" + id + '\'' +
                "  firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                '}';
    }

    public User(int id, String firstname, String lastname, String phone, String address) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.phone = phone;
        this.address = address;
    }
}
